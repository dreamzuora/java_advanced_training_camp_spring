package net.csdn.dreamzuora.service;

/**
 * @author weijie
 * @date 2020/3/9 22:12
 */
public interface TransferService {
    void transfer(String fromUser, String toUser, int money) throws Exception;
}
