package net.csdn.dreamzuora.service.impl;

import net.csdn.dreamzuora.annotation.Autowired;
import net.csdn.dreamzuora.annotation.Service;
import net.csdn.dreamzuora.annotation.Transactional;
import net.csdn.dreamzuora.dao.AccountDao;
import net.csdn.dreamzuora.pojo.AccountDto;
import net.csdn.dreamzuora.service.TransferService;

/**
 * @author weijie
 * @date 2020/3/9 22:12
 */
@Service("transferService")
@Transactional
public class TransferServiceImpl implements TransferService {

    @Autowired
    private AccountDao accountDao;


    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }



    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {
        AccountDto from = accountDao.queryAccountByCardNo(fromCardNo);
        AccountDto to = accountDao.queryAccountByCardNo(toCardNo);

        from.setMoney(from.getMoney()-money);
        to.setMoney(to.getMoney()+money);

        accountDao.updateAccountByCardNo(to);
        //触发事务回滚机制
//        int i = 1/0;
        accountDao.updateAccountByCardNo(from);

    }
}
