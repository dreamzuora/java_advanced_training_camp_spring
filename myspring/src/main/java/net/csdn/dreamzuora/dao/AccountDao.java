package net.csdn.dreamzuora.dao;


import net.csdn.dreamzuora.pojo.AccountDto;

/**
 * @author 应癫
 */
public interface AccountDao {

    AccountDto queryAccountByCardNo(String cardNo) throws Exception;

    int updateAccountByCardNo(AccountDto account) throws Exception;
}
