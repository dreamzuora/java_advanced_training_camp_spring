package net.csdn.dreamzuora.utils;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * @author weijie
 * @date 2020/3/9 22:28
 */
public class DruidUtils {

    private static DruidDataSource druidDataSource = new DruidDataSource();

    static {
        druidDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/wj_mybatis");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("123456");
    }

    public static DruidDataSource getInstance(){
        return druidDataSource;
    }
}
