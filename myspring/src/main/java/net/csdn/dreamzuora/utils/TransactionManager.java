package net.csdn.dreamzuora.utils;

import net.csdn.dreamzuora.annotation.Autowired;
import net.csdn.dreamzuora.annotation.Component;

import java.sql.SQLException;

/**
 * @author weijie
 * @date 2020/3/9 22:25
 */
@Component
public class TransactionManager {

    @Autowired
    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils){
        this.connectionUtils = connectionUtils;
    }

    //开启手动事务控制
    public void beginTransaction() throws SQLException {
        connectionUtils.getCurrentThreadConnection().setAutoCommit(false);
    }

    //提交事务
    public void commit() throws SQLException{
        connectionUtils.getCurrentThreadConnection().commit();
    }

    // 回滚事务
    public void rollback() throws SQLException {
        connectionUtils.getCurrentThreadConnection().rollback();
    }
}
