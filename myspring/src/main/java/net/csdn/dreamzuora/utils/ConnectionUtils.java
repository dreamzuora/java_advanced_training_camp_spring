package net.csdn.dreamzuora.utils;

import net.csdn.dreamzuora.annotation.Autowired;
import net.csdn.dreamzuora.annotation.Component;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author weijie
 * @date 2020/3/9 22:24
 */
@Component
public class ConnectionUtils {
    //存储当前线程的连接
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    @Autowired
    public TransactionManager transactionManager;

    /**
     * 从当前线程获取连接
     * @return
     */
    public Connection getCurrentThreadConnection() throws SQLException {
        Connection connection = threadLocal.get();
        if (connection == null){
            //从连接池拿连接并绑定到线程
            connection = DruidUtils.getInstance().getConnection();
            //绑定到当前线程
            threadLocal.set(connection);
        }
        return connection;
    }
}
