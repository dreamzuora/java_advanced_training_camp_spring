package net.csdn.dreamzuora.servlet;

import net.csdn.dreamzuora.factory.BeanFactory;
import net.csdn.dreamzuora.pojo.ResultSet;
import net.csdn.dreamzuora.pojo.ResultSetCode;
import net.csdn.dreamzuora.service.TransferService;
import net.csdn.dreamzuora.utils.JsonUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author weijie
 * @date 2020/3/9 22:07
 */
@WebServlet(name="transferServlet",urlPatterns = "/transferServlet")
public class TransferServlet extends HttpServlet {
    private TransferService transferService = (TransferService) BeanFactory.getBean("transferService") ;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String fromCardNo = req.getParameter("fromCardNo");
        String toCardNo = req.getParameter("toCardNo");
        String moneyStr = req.getParameter("money");
        int money = Integer.parseInt(moneyStr);
        ResultSet<Object> resultSet = null;
        try {
            transferService.transfer(fromCardNo,toCardNo,money);
            resultSet = ResultSet.valueOf(ResultSetCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            resultSet = ResultSet.valueOf(ResultSetCode.SYSTEM_ERROR);
            resultSet.setResult(e);
        }
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(resultSet));
    }
}
