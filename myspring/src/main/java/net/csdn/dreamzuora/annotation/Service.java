package net.csdn.dreamzuora.annotation;

import java.lang.annotation.*;

/**
 * @author weijie
 * @date 2020/3/9 22:06
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {
    String value() default "";
}
