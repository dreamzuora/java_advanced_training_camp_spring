package net.csdn.dreamzuora.annotation;

import java.lang.annotation.*;

/**
 * @author weijie
 * @date 2020/3/9 22:05
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Component {
    String value() default "";
}
