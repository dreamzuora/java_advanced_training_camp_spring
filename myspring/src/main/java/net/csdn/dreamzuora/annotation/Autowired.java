package net.csdn.dreamzuora.annotation;

import java.lang.annotation.*;

/**
 * @author weijie
 * @date 2020/3/9 22:05
 */
@Documented//表明该注解标记的元素可以被Javadoc 或类似的工具文档化
@Target(ElementType.FIELD)//该注解可以应用字段、枚举的常量
@Retention(RetentionPolicy.RUNTIME)//注解会在class字节码文件中存在，在运行时可以通过反射获取到
public @interface Autowired {
}
