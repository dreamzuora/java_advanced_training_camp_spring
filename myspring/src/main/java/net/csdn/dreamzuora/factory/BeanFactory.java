package net.csdn.dreamzuora.factory;

import net.csdn.dreamzuora.annotation.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author weijie
 * @date 2020/3/9 22:50
 */
public class BeanFactory {
    //缓存扫描到的class全限定类名
    static List<String> classNemsList = new ArrayList<>();
    static List<String> fieldsReadyList = new ArrayList<>();
    //存储对象
    private static Map<String, Object> beansMap = new HashMap<>();

    static {
        try {
            doParse();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void doParse() throws DocumentException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(resourceAsStream);
        Element rootElement = document.getRootElement();
        Element scanElement = (Element)rootElement.selectSingleNode("//component-scan");
        String scanPackage = scanElement.attributeValue("base-package");
        //扫描注解
        scanClassOfPackage(scanPackage);
        //实例化
        doInstance();
        //维护依赖注入关系
        doAutowired();
        //维护事务
        doTransactional();
    }

    /**
     * 扫描注解
     * @param scanPackage
     */
    private static void scanClassOfPackage(String scanPackage){
        String scanPackagePath = Thread.currentThread().getContextClassLoader().getResource("").getPath() + scanPackage.replaceAll("\\.", "/");
        File packageFile = new File(scanPackagePath);
        File[] files = packageFile.listFiles();
        for (File file : files){
            if (file.isDirectory()){
                scanClassOfPackage(scanPackage + "." + file.getName());
            }else if (file.getName().endsWith(".class")){
                String className = scanPackage + "." + file.getName().replaceAll(".class", "");
                classNemsList.add(className);
            }
        }
    }

    /**
     * 实例化
     */
    private static void doInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if (classNemsList.size() == 0)
            return;

        for (int i = 0, len = classNemsList.size(); i < len; i++){
            String className = classNemsList.get(i);
            Class<?> aClass = Class.forName(className);
            if (aClass.isAnnotationPresent(Service.class) || aClass.isAnnotationPresent(Repository.class) || aClass.isAnnotationPresent(Component.class)){
                //获取注解value值
                String beanName = null;

                if (aClass.isAnnotationPresent(Service.class)){
                    beanName = aClass.getAnnotation(Service.class).value();
                }else if (aClass.isAnnotationPresent(Repository.class)){
                    beanName = aClass.getAnnotation(Repository.class).value();
                }else if (aClass.isAnnotationPresent(Component.class)){
                    beanName = aClass.getAnnotation(Component.class).value();
                }
                // 如果指定了id，就以指定的为准
                Object obj = aClass.newInstance();
                if ("".equals(beanName)){
                    //aClass.getSimpleName():获取类的简写名称
                    beanName = lowerFirst(aClass.getSimpleName());
                }
                beansMap.put(beanName, obj);
                // service层往往是有接口的，面向接口开发，此时再以接口名为id，放入一份对象到容器中，便于后期根据接口类型注入
                Class<?>[] interfaces = aClass.getInterfaces();
                if (interfaces != null && interfaces.length > 0){
                    for (int j = 0, interLen = interfaces.length; j < interLen; j++){
                        Class<?> anInterface = interfaces[j];
                        //以接口的全限定类名作为id放入
                        beansMap.put(anInterface.getName(), aClass.newInstance());
                    }
                }

            }
        }
    }
    /**
     * 首字母小写方法
     */
    private static String lowerFirst(String str) {
        char[] chars = str.toCharArray();
        if('A' <= chars[0] && chars[0] <= 'Z') {
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }
    /**
     * 维护依赖注入关系
     */
    private static void doAutowired() throws IllegalAccessException {
        if (beansMap.isEmpty())
            return;
        for (Map.Entry<String, Object> entry : beansMap.entrySet()){
            System.out.println("entry.getValue():" + entry.getValue());
            doObjectDependancy(entry.getValue());
        }
    }

    /**
     * 维护事务,为添加了@MyTransactional注解的对象创建代理对象，并覆盖原IOC容器中的对象
     */
    private static void doTransactional(){
        ProxyFactory proxyFactory = (ProxyFactory) beansMap.get("proxyFactory");
        for(Map.Entry<String,Object> entry: beansMap.entrySet()) {
            String beanName = entry.getKey();
            Object o = entry.getValue();
            Class<?> aClass = entry.getValue().getClass();
            if(aClass.isAnnotationPresent(Transactional.class)) {
                // 需要进行事务控制

                // 有实现接口
                Class<?>[] interfaces = aClass.getInterfaces();
                if(interfaces != null && interfaces.length > 0) {
                    // 使用jdk动态代理
                    beansMap.put(beanName,proxyFactory.getJdkProxy(o));
                }else{
                    // 使用cglib动态代理
                    beansMap.put(beanName,proxyFactory.getCglibProxy(o));
                }
            }
        }
    }

    /**
     * A 可能依赖于 B ，B 可能依赖于 C ，C 可能又依赖于D，本方法主要维护一下嵌套依赖
     */
    private static void doObjectDependancy(Object object) {
        System.out.println(object);
        Field[] declaredFields = object.getClass().getDeclaredFields();

        if (declaredFields == null || declaredFields.length == 0){
            return;
        }

        for (int i = 0, fieldLen = declaredFields.length; i < fieldLen; i++){
            Field declaredField = declaredFields[i];

            if (!declaredField.isAnnotationPresent(Autowired.class)){
                continue;
            }

            /**
             * 判断当前字段是否处理过，如果已经处理过则跳过，避免嵌套处理bean造成死循环
             */
            if (fieldsReadyList.contains(object.getClass().getName() + "." + declaredField.getName())){
                continue;
            }

            Object dependObject = null;
            dependObject = beansMap.get(declaredField.getType().getName());  //  先按照声明的是接口去获取，如果获取不到再按照首字母小写

            if(dependObject == null) {
                dependObject = beansMap.get(lowerFirst(declaredField.getType().getSimpleName()));
            }

            // 记录下给哪个对象的哪个属性设置过，避免死循环
            fieldsReadyList.add(object.getClass().getName() + "." + declaredField.getName());

            // 迭代
            doObjectDependancy(dependObject);

            declaredField.setAccessible(true);
            try {
                declaredField.set(object,dependObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    // 对外提供获取实例对象的接口（根据id获取）
    public static  Object getBean(String id) {
        return beansMap.get(id);
    }
}
